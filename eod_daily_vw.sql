CREATE VIEW users_pmi.eod_daily_vw
(
    metric,
    model_grp,
    report_dt,
    measure
)
AS
With variable_tbl AS ( 
  -- Update these variables Beginning of each Year and Quarter
  SELECT
  '2024-12-31'::DATE AS trgt_end_date,
  '2024-01-1 00:00:00'::TIMESTAMP AS qrt_start_date,
  '2024-12-31 00:00:00'::TIMESTAMP AS delivery_appointment_end_date
--   'Targets_11_13_2023'::VARCHAR AS target_scenario
)

, base_data as 
(
  with projection as
(
  select shop,
       sum(target) as projection_daily --CONCAT('Projection_',scenario)
  from users_pmi.daily_target
  where date_planned = date(CONVERT_TIMEZONE('US/Pacific', getdate())) --current_date 
    and date <  (select trgt_end_date from variable_tbl) and 
    --scenario like 'Projections%'
    scenario = 'Projections_'+ CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) +'_'
+ CAST(DATE_PART(day,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) + '_' 
+ CAST(DATE_PART(year,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR )
    and shop in ('Delivery')
  group by shop

)
, QTD_Delivered as (
  select 
  'QTD Delivered' AS Metric,
  CASE WHEN Model like 'R1%' THEN 'R1' Else Model End as model_grp, 
  count(distinct vin) as vin_cnt
  from users_pmi.Delivery_Main_Walk4_vw
  where variable = 'Factory Gate Date'
  AND delivery_date_pst_core2 BETWEEN (select CAST(qrt_start_date AS TIMESTAMP) from variable_tbl) AND getdate()
  GROUP BY Metric,model_grp
)
SELECT 
  'QTD Projection' AS Metric,
  model_grp,
   DATE(convert_timezone('US/Pacific',GETDATE())) as report_dt,
  CASE WHEN model_grp = 'R1' THEN round(vin_cnt + projection_daily + 118,2) -- AS of 12/5/2023 sale lever = 900/21 + 75 adding 75$ for B2B to o900$ sales lever
  ELSE Round(1296, 2) END AS measure
FROM QTD_Delivered
cross join projection

UNION ALL

/* Delivered */
/* EDV Delivered*/
SELECT
  'Delivered' AS Metric,
  'EDV' AS model_grp,
  date(cdd.delivery_timestamp_pst) as report_dt,
  COUNT(DISTINCT A.vin) AS measure
FROM commercial_core_data.ccd_edv_vin_tracker A
INNER JOIN commercial_core_data.ccd_delivery_date cdd ON a.vin = cdd.vin
WHERE date(cdd.delivery_timestamp_pst)  BETWEEN CURRENT_DATE - INTERVAL '1 year' AND CURRENT_DATE
Group By report_dt

UNION ALL
/* R1 Delivered */
-- lower than KPI (70)
SELECT
  'Delivered' AS Metric,
  'R1' AS model_grp,
  date(delivery_date) as report_dt,
  COUNT(DISTINCT a.vin) AS measure
FROM commercial_core_data.ccd_vin_universe A
INNER JOIN commercial_core_data.ccd_delivery_date cdd ON a.vin = cdd.vin
WHERE vehicle_usage IN ('CUSTOMER_DELIVERY')
   --AND ccd_vin_salability = 'SALEABLE'
   AND date(delivery_date)  BETWEEN CURRENT_DATE - INTERVAL '1 year' AND CURRENT_DATE
GROUP BY report_dt

UNION ALL

select 
  'Delivered B2B' AS Metric,
  'R1' AS model_grp,
  date(cdd.delivery_timestamp_pst) as report_dt,
  COUNT(DISTINCT b2b.vin) as measure
from commercial_core_data.ccd_b2b_vin_tracker as b2b
INNER JOIN commercial_core_data.ccd_delivery_date cdd ON b2b.vin = cdd.vin
WHERE date(cdd.delivery_timestamp_pst)  BETWEEN CURRENT_DATE - INTERVAL '1 year' AND CURRENT_DATE
GROUP BY report_dt
   

-- UNION ALL
-- /* Scheduled*/
-- SELECT
--   'Scheduled' AS Metric,
--   'R1' AS model_grp,
--   date(B.t2d_delivery_appointment_date_pst) as report_dt,
--   COUNT(A.vin) AS measure
-- FROM commercial_core_data.ccd_vin_universe A
-- LEFT OUTER JOIN commercial_core_data.ccd_current_delivery_appointment B ON A.vin = B.vin
-- WHERE A.delivery_date IS NULL
--     AND A.ccd_vehicle_usage IN ('CUSTOMER_DELIVERY', 'RETURNED')
-- GROUP BY report_dt

UNION ALL
/*Targets by Shop Today*/ -- needs work to align daily targets
select
-- CAST(convert_timezone('US/Pacific',getdate()) AS DATE) as Report_date, 
  case when shop = 'Matching' then CONCAT(shop,CONCAT(' - ',CONCAT(engine_type,' Target'))) else CONCAT(shop, ' Target') end AS Metric,
  'R1' AS model_grp,
  date(CONVERT_TIMEZONE('US/Pacific', getdate())) as report_dt,
  Round(sum(target),0) AS measure
from users_pmi.daily_target
where date =  date(CONVERT_TIMEZONE('US/Pacific', getdate())) --current_date 
and date_planned = cast(dateadd('Day',-1,CONVERT_TIMEZONE('US/Pacific', getdate())) as date)
and scenario =  
'Targets_' + case when EXTRACT(DAY FROM date(CONVERT_TIMEZONE('US/Pacific', getdate()))) = 1
THEN LPAD(EXTRACT(Month FROM date_add('day', -1 ,CONVERT_TIMEZONE('US/Pacific', getdate())))::VARCHAR, 2, '0') 
else 
LPAD(EXTRACT(Month FROM CONVERT_TIMEZONE('US/Pacific', getdate()))::VARCHAR, 2, '0') END +'_'
+ LPAD(EXTRACT(Day FROM date_add('day', -1 ,CONVERT_TIMEZONE('US/Pacific', getdate())))::VARCHAR, 2, '0') +'_'

+ CAST(DATE_PART(year,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR ) 
/*
'Targets_' + case when EXTRACT(DAY FROM date(CONVERT_TIMEZONE('US/Pacific', getdate()))) = 1
THEN CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate())))-1 AS VARCHAR)
else CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) END +'_'
--CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) +'_'
+ CAST(DATE_PART(day,date(dateadd(day, -1, CONVERT_TIMEZONE('US/Pacific', getdate())))) AS VARCHAR) + '_' 
+ CAST(DATE_PART(year,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR ) 
--trgt_scenario --'Targets_10_31_2023' -- use scenario date today - 1day
*/
and shop not in ('Factory Gate','Arrived at SC','Ingress')
group by Metric, model_grp, report_dt

UNION ALL 
/*Targets by Shop Yesterday - Pull Saturday Targets on Mondays*/
-- select
-- -- CAST(convert_timezone('US/Pacific',getdate()) AS DATE) as Report_date, 
--   case when shop = 'Matching' then CONCAT(shop,CONCAT(' - ',CONCAT(engine_type,' Target'))) else CONCAT(shop, ' Target') end AS Metric,
--   'R1' AS model_grp,
--   date(dateadd(day, -1,CONVERT_TIMEZONE('US/Pacific', getdate()))) as report_dt,
--   Round(sum(target),0) AS measure
-- from users_pmi.daily_target
-- where date =  date(CONVERT_TIMEZONE('US/Pacific', getdate())) --current_date 
-- and scenario = 'Targets_' + CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) +'_'
-- + CAST(DATE_PART(day,date(dateadd(day, -2, CONVERT_TIMEZONE('US/Pacific', getdate())))) AS VARCHAR) + '_' 
-- + CAST(DATE_PART(year,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR )
-- --trgt_scenario --'Targets_10_31_2023' -- use scenario date today - 1day
-- and shop not in ('Factory Gate','Arrived at SC','Ingress') and date =  date(CONVERT_TIMEZONE('US/Pacific', getdate())) --current_date
-- group by Metric, model_grp, report_dt

/*Targets by Shop Yesterday*/ 

select
-- CAST(convert_timezone('US/Pacific',getdate()) AS DATE) as Report_date, 
  case when shop = 'Matching' then CONCAT(shop,CONCAT(' - ',CONCAT(engine_type,' Target'))) else CONCAT(shop, ' Target') end AS Metric,
  'R1' AS model_grp,
  date(dateadd(day, -1,CONVERT_TIMEZONE('US/Pacific', getdate()))) as report_dt,
  Round(sum(target),0) AS measure
from 
( select * , 
         case when extract(DOW FROM CONVERT_TIMEZONE('US/Pacific',GETDATE())) = 1
    THEN 
      -- if Monday
       'Targets_' + 
       case when EXTRACT(DAY FROM date(CONVERT_TIMEZONE('US/Pacific', getdate()))) = 1
THEN 
LPAD(EXTRACT(Month FROM date_add('day', -1 ,CONVERT_TIMEZONE('US/Pacific', getdate())))::VARCHAR, 2, '0')
--CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate())))-1 AS VARCHAR)
else 
  LPAD(EXTRACT(Month FROM CONVERT_TIMEZONE('US/Pacific', getdate()))::VARCHAR, 2, '0') END

--CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) END
        +'_'
+ LPAD(EXTRACT(Day FROM date_add('day', -3 ,CONVERT_TIMEZONE('US/Pacific', getdate())))::VARCHAR, 2, '0') + '_' 
--CAST(DATE_PART(day,date(dateadd(day, -3, CONVERT_TIMEZONE('US/Pacific', getdate())))) AS VARCHAR) + '_' 
+ CAST(DATE_PART(year,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR)
     
  else
      'Targets_' + case when EXTRACT(DAY FROM date(CONVERT_TIMEZONE('US/Pacific', getdate()))) = 1
THEN 
  LPAD(EXTRACT(Month FROM date_add('day', -1 ,CONVERT_TIMEZONE('US/Pacific', getdate())))::VARCHAR, 2, '0')
--CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate())))-1 AS VARCHAR)
else 
   LPAD(EXTRACT(Month FROM CONVERT_TIMEZONE('US/Pacific', getdate()))::VARCHAR, 2, '0') END +'_'
--CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) END +'_'
     --CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) +'_'
+ LPAD(EXTRACT(Day FROM date_add('day', -2 ,CONVERT_TIMEZONE('US/Pacific', getdate())))::VARCHAR, 2, '0') + '_' 
--CAST(DATE_PART(day,date(dateadd(day, -2, CONVERT_TIMEZONE('US/Pacific', getdate())))) AS VARCHAR) + '_' 
+ CAST(DATE_PART(year,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR)
  END as scenario_adjusted
/* case when extract(DOW FROM CONVERT_TIMEZONE('US/Pacific',GETDATE())) = 1
    THEN 
      -- if Monday
       'Targets_' + 
       case when EXTRACT(DAY FROM date(CONVERT_TIMEZONE('US/Pacific', getdate()))) = 1
THEN CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate())))-1 AS VARCHAR)
else CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) END
        +'_'
+ CAST(DATE_PART(day,date(dateadd(day, -3, CONVERT_TIMEZONE('US/Pacific', getdate())))) AS VARCHAR) + '_' 
+ CAST(DATE_PART(year,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR)
     
  else
      'Targets_' + case when EXTRACT(DAY FROM date(CONVERT_TIMEZONE('US/Pacific', getdate()))) = 1
THEN CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate())))-1 AS VARCHAR)
else CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) END +'_'
     --CAST(DATE_PART(month,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR) +'_'
+ CAST(DATE_PART(day,date(dateadd(day, -2, CONVERT_TIMEZONE('US/Pacific', getdate())))) AS VARCHAR) + '_' 
+ CAST(DATE_PART(year,date(CONVERT_TIMEZONE('US/Pacific', getdate()))) AS VARCHAR )
  END as scenario_adjusted
  */

  from users_pmi.daily_target as dt
  where scenario like 'Targets_%'
   and (/* On Mondays use these date filters */
    case when extract(DOW FROM CONVERT_TIMEZONE('US/Pacific',GETDATE())) = 1
  THEN 
  date_planned = cast(dateadd('Day',-3,CONVERT_TIMEZONE('US/Pacific', getdate())) as date) -- as of Friday
  and dt.date = cast(dateadd('Day',-2,CONVERT_TIMEZONE('US/Pacific', getdate())) as date) -- date always starts 1 day after target scenario for each day
  ELSE true END)
  /* weekdays Except Monday */
  and (case when extract(DOW FROM CONVERT_TIMEZONE('US/Pacific',GETDATE())) <> 1
  THEN 
  date_planned = cast(dateadd('Day',-2,CONVERT_TIMEZONE('US/Pacific', getdate())) as date) -- as of Friday
  and dt.date = cast(dateadd('Day',-1,CONVERT_TIMEZONE('US/Pacific', getdate())) as date) -- date always starts 1 day after target scenario for each day
  else true END
  )
  
  and shop not in ('Factory Gate','Arrived at SC','Ingress') 
) sa

where  
scenario = scenario_adjusted
--trgt_scenario --'Targets_10_31_2023' -- use scenario date today - 1day
group by Metric, model_grp, report_dt


UNION ALL
/* Actual Performance  */ 
/* Shipped */
SELECT  
  'Shipped' AS Metric,
  'R1' AS model_grp,
  date(B.actual_departure) as report_dt,
COUNT(a.vin) AS measure
FROM commercial_core_data.ccd_vin_universe a
INNER JOIN commercial_core_data.ccd_shipment_info B ON A.vin = B.rivian_vin_normal_first_ship
WHERE 
   a.ccd_vehicle_usage IN ('CUSTOMER_DELIVERY', 'RETURNED')
   AND  date(B.actual_departure) BETWEEN CURRENT_DATE - INTERVAL '1 year' AND CURRENT_DATE
GROUP BY report_dt

UNION ALL
/* FG */
-- Higher than Factory View
SELECT 
  'Factory Gated' AS Metric,
  'R1' AS model_grp,
  date(CONVERT_TIMEZONE('US/Central', factory_gate_date)) as report_dt,
  COUNT(a.vin) AS measure
FROM commercial_core_data.ccd_vin_universe a
  WHERE 
    a.ccd_vehicle_usage IN ('CUSTOMER_DELIVERY', 'RETURNED')
    AND  date(CONVERT_TIMEZONE('US/Central', factory_gate_date)) BETWEEN CURRENT_DATE - INTERVAL '1 year' AND CURRENT_DATE
GROUP BY report_dt

/* Matching-R1 Shop & Mathcing-Direct Actuals */ -- replaced by below FG with a match & post FG Match
UNION ALL 

SELECT 
   measure_name AS Metric,
   'R1' AS model_grp,
   report_dt,
  vin_count as measure
FROM (

  SELECT date(CONVERT_TIMEZONE('US/Pacific', a.factory_gate_date)) AS report_dt
    , 'FG with a match' as measure_name
    , COUNT(a.vin) as vin_count
FROM commercial_core_data.ccd_vin_universe a
        INNER JOIN commercial_core_data.ccd_vin_match_status cvms ON a.vehicle_id = cvms.vehicle_id
WHERE 
  ccd_vehicle_usage IN ('CUSTOMER_DELIVERY', 'RETURNED') --Only cust vehicles
  AND a.ccd_vehicle_stage <> 'Pre FG' -- Getting only FG'd and Delivered Units
  AND date(CONVERT_TIMEZONE('US/Pacific', a.factory_gate_date)) >=
      date(dateadd(day, -8, CONVERT_TIMEZONE('US/Pacific', GETDATE()))) --Take FG'd Units with real a match in past 8 days
  AND cvms.match_status = 'REAL' --Only real matches
GROUP BY 1

UNION ALL

SELECT date(match_date_pst) AS report_dt
    , 'Post-FG Match'  AS measure_name
    , COUNT(a.vin) AS vin_count
FROM commercial_core_data.ccd_vin_universe a
        INNER JOIN commercial_core_data.ccd_vin_match_status cvms ON a.vehicle_id = cvms.vehicle_id
WHERE 
  ccd_vehicle_usage IN ('CUSTOMER_DELIVERY', 'RETURNED') --Only cust vehicles
  AND a.ccd_vehicle_stage <> 'Pre FG' -- Getting only FG'd and Delivered Units
  AND date(match_date_pst) >=
      date(dateadd(day, -8, CONVERT_TIMEZONE('US/Pacific', GETDATE()))) --Take matched Units past 8 days
  AND cvms.match_status = 'REAL' --Only real matches
  AND date(CONVERT_TIMEZONE('US/Pacific', a.factory_gate_date)) <
      date(CONVERT_TIMEZONE('US/Pacific', GETDATE())) --FG can only be in be in the past
GROUP BY 1


) as matching_shop_base

UNION ALL
/* PDI Start Actual Today & Yesterday */
-- higher than KPI (10)
SELECT 
  'PDI Started' AS Metric,
  'R1' AS model_grp,
  date(CONVERT_TIMEZONE('US/Pacific', pdi_start_date)) as report_dt,
  COUNT(a.vin) AS measure
FROM commercial_core_data.ccd_vin_universe a
WHERE  
  a.ccd_vehicle_usage IN ('CUSTOMER_DELIVERY', 'RETURNED')
  -- AND ccd_vin_salability = 'SALEABLE'
  AND ccd_vehicle_stage = 'Post FG'
  AND  date(CONVERT_TIMEZONE('US/Pacific', pdi_start_date)) BETWEEN CURRENT_DATE - INTERVAL '1 year' AND CURRENT_DATE
GROUP BY report_dt

UNION ALL
/* PDI Completed Actual Today & Yesterday */
SELECT  
  'PDI Completed' AS Metric,
  'R1' AS model_grp,
  date(CONVERT_TIMEZONE('US/Pacific', pdi_complete_date)) as report_dt,
  COUNT(a.vin) AS measure
FROM commercial_core_data.ccd_vin_universe a
WHERE  date(CONVERT_TIMEZONE('US/Pacific', pdi_complete_date)) >= date(DATEADD(day,-1,CONVERT_TIMEZONE('US/Pacific', GETDATE())))
  AND a.ccd_vehicle_usage IN ('CUSTOMER_DELIVERY', 'RETURNED')
GROUP BY report_dt



UNION ALL 
SELECT  
  --  CAST(convert_timezone('US/Pacific',getdate()) AS DATE) as Report_date,
  'Scheduled' AS Metric,
  'R1' AS model_grp,
  -- date(apt_data.created_at) AS cut_off_date,
  date(apt_data.created_at) AS report_dt,
  -- NULL AS shop_sale_channel,
  count(apt_data.rivian_id) AS measure
  -- convert_timezone('US/Pacific',GETDATE()) as load_time_stamp
FROM 
(
  with main as (select  
		rivian_id,
		location_id , 
    orderid,
		appointment_id,
		convert_timezone('US/Pacific', created_at) as created_at,
		convert_timezone('US/Pacific', start_at) as start_at , 
		convert_timezone('US/Pacific', end_at) as end_at,
		act.appt_description, 
		loc.appt_location_name,
		appointment_status,
        ROW_NUMBER() OVER (PARTITION by rivian_id  ORDER BY created_at DESC  ) AS rn_apt
	 from commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_appointment apt
	inner join
		(select activity_id as activity_id_act_def, 
				description as appt_description
			from
				commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_activity
			where
				activity_state = 'IN_OPERATION' and
				activity_type = 'VEHICLE_DELIVERY'
		) act
		on act.activity_id_act_def = apt.activity_id
	inner join 
		(select
			location_id as location_id_loc , 
			location_name as appt_location_name
		from commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_location) loc
			on loc.location_id_loc = apt.location_id
       where 
      		appointment_status in ('SCHEDULED','IN_PROGRESS','COMPLETED'))
,

cancel_count as (
select rivian_id as rivian_id_cancel_count ,count(*) as cancel_count from 
(
		select
		rivian_id,
		location_id,
    orderid,
		appointment_id,
		convert_timezone('US/Pacific', created_at) as created_at,
		convert_timezone('US/Pacific', start_at) as start_at , 
		convert_timezone('US/Pacific', end_at) as end_at,
		act.appt_description, 
		loc.appt_location_name,
		appointment_status,
        ROW_NUMBER() OVER (PARTITION by rivian_id  ORDER BY created_at DESC  ) AS rn_apt
	 from commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_appointment apt
	inner join
		(select activity_id as activity_id_act_def, 
				description as appt_description
			from
				commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_activity
			where
				activity_state = 'IN_OPERATION' and
				activity_type = 'VEHICLE_DELIVERY'
		) act
		on act.activity_id_act_def = apt.activity_id
	inner join 
		(select
			location_id as location_id_loc , 
			location_name as appt_location_name
		from commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_location) loc
			on loc.location_id_loc = apt.location_id
       where 
      		appointment_status not in ('SCHEDULED','IN_PROGRESS','COMPLETED')
      		)
          
group by rivian_id),

recent_cancel as (
  select rivian_id as recent_cancel_rivid, 
        location_id as recent_cancel_locationid, appointment_id as rc_appointmentid, orderid as rc_orderid,
        created_at as recent_create,
		start_at as recent_start_at,
        end_at as recent_end_at,
        appt_description as recent_appt_description,
        appt_location_name as recent_appt_location_name,
        appointment_status as recent_appointment_status
  from 
(
		select
		rivian_id,
		location_id,
    orderid,
		appointment_id,
		convert_timezone('US/Pacific', created_at) as created_at,
		convert_timezone('US/Pacific', start_at) as start_at , 
		convert_timezone('US/Pacific', end_at) as end_at,
		act.appt_description, 
		loc.appt_location_name,
		appointment_status,
        ROW_NUMBER() OVER (PARTITION by rivian_id,orderid  ORDER BY created_at asc  ) AS rn_apt
	 from commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_appointment apt
	inner join
		(select activity_id as activity_id_act_def, 
				description as appt_description
			from
				commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_activity
			where
				activity_state = 'IN_OPERATION' and
				activity_type = 'VEHICLE_DELIVERY'
		) act
		on act.activity_id_act_def = apt.activity_id
	inner join 
		(select
			location_id as location_id_loc , 
			location_name as appt_location_name
		from commercial_ops_datashare_db.rivian_ams_ods.coresvcs_ams_location) loc
			on loc.location_id_loc = apt.location_id
       where 
      		appointment_status not in ('SCHEDULED','IN_PROGRESS','COMPLETED')
      		)
where rn_apt=1
)

select 
  *
from main
left join cancel_count cc
on cc.rivian_id_cancel_count = rivian_id
left join recent_cancel as rc
on rc.recent_cancel_rivid= rivian_id and rc.recent_cancel_locationid = location_id 
and rc.rc_orderid = orderid
left join  commercial_ops_datashare_db.rivn_salesforce.order__c as ps on ps.order_number__c = orderid
  where 
   delivery_scheduling_outreach_status__c != 'Preemptively Scheduled'
  

) AS apt_data
WHERE  date(created_at) >= date(dateadd(day,-1,CONVERT_TIMEZONE('US/Pacific', getdate())))
  AND ( date(recent_create) IS NULL or  date(recent_create) = date(dateadd(day,-1,CONVERT_TIMEZONE('US/Pacific', getdate()))))
GROUP BY report_dt
)
select * 
from base_data
where report_dt >= Current_DATE - INTERVAL '30 days'
with no schema binding;